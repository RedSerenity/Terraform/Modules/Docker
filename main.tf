locals {
	caddy_proxy = tomap({
		"caddy" = "${var.domain.domain_name}", //"
		"caddy.reverse_proxy" = "{{ upstreams ${var.domain.port} }}"
	})
}

resource "docker_container" "container" {
	name = var.name
	image = var.image

	start = var.start
	restart = var.restart

	command = var.command

	dynamic "networks_advanced" {
		for_each = var.networks
		iterator = each
		content {
			name = each.value.name
			aliases = each.value.aliases
		}
	}

	dynamic "ports" {
		for_each = var.ports
		iterator = each
		content {
			internal = each.value.internal
			external = each.value.external
			protocol = each.value.protocol
		}
	}

	dynamic "volumes" {
		for_each = var.volumes
		iterator = each
		content {
			host_path = each.value.host_path
			container_path = each.value.container_path
			read_only = each.value.read_only
		}
	}

	dynamic "volumes" {
		for_each = var.volume_names
		iterator = each
		content {
			volume_name = each.value.volume_name
			container_path = each.value.container_path
			read_only = each.value.read_only
		}
	}

	env = [for key, value in var.environment: "${key}=${value}"]

	dynamic "labels" {
		for_each = var.labels
		iterator = each
		content {
			label = each.key
			value = each.value
		}
	}

	dynamic "labels" {
		for_each = var.domain.port > 0 ? local.caddy_proxy : tomap({})
		iterator = each
		content {
			label = each.key
			value = each.value
		}
	}

	dynamic "labels" {
		for_each = length(var.stack) > 0 ? tomap({ "com.docker.compose.project" = var.stack }) : tomap({})
		iterator = each
		content {
			label = each.key
			value = each.value
		}
	}

	dynamic "devices" {
		for_each = var.devices
		iterator = each
		content {
			host_path = each.value.device
			container_path = coalesce(each.value.mapTo, "/dev/ttyUSB0")
		}
	}

	dynamic "capabilities" {
		for_each = length(var.capabilities.add) > 0 ? tomap({ "caps" = toset(var.capabilities.add)}) : tomap({ "caps" = []})
		iterator = each
		content {
			add = each.value
		}
	}
}