variable "name" {
  type = string
}

variable "image" {
  type = string
}

variable "command" {
  type = list(string)
  default = []
}

variable "restart" {
	type = string
	default = "unless-stopped"
}

variable "start" {
	type = bool
	default = true
}

variable "networks" {
  type = list(object({
    name = string
    aliases = list(string)
  }))
  default = []
}

variable "ports" {
  type = list(object({
    internal = number
    external = number
    protocol = string
  }))
  default = []
}

variable "volumes" {
  type = list(object({
    host_path = string
    container_path = string
    read_only = bool
  }))
  default = []
}

variable "volume_names" {
  type = list(object({
    volume_name = string
    container_path = string
    read_only = bool
  }))
  default = []
}

variable "environment" {
  type = map(string)
  default = {}
}

variable "labels" {
  type = map(string)
  default = {}
}

variable "domain" {
  type = object({
    domain_name = string
    port = number
  })
  default = {
    domain_name = null
    port = 0
  }
}

variable "stack" {
	type = string
	default = null
}

variable "devices" {
	type = list(object({
		device = string
		mapTo = string
	}))
	default = []
}

variable "capabilities" {
	type = object({
		add = list(string)
		drop = list(string)
	})
	default = {
		add = []
		drop = []
	}
}